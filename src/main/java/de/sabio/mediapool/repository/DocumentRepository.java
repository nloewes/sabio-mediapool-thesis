package de.sabio.mediapool.repository;

import de.sabio.mediapool.model.DocumentEntity;

import de.sabio.mediapool.model.Folder;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.ArrayList;

/**
 * DAO for the Document entity
 *
 * @author nloewes
 */
public interface DocumentRepository extends CrudRepository<DocumentEntity,Integer> {

    //search by exact title match
    @Query("SELECT d FROM DocumentEntity d WHERE d.title=:title")
    ArrayList<DocumentEntity> findByTitle(@Param("title") String title);

    //search by string containment
    ArrayList<DocumentEntity> findByTitleContaining(String title);

    //wildcard search
    ArrayList<DocumentEntity> findByTitleLike(String title);

    //find all documents for a given parent Folder
    ArrayList<DocumentEntity> findByParent(Folder parent);
}
