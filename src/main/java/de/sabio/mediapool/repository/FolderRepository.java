package de.sabio.mediapool.repository;

import de.sabio.mediapool.model.Folder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * DAO for the Folder entity
 *
 * @author nloewes
 */
public interface FolderRepository extends JpaRepository<Folder, Integer> {

    @Query("SELECT f FROM Folder f INNER JOIN f.parentFolder p WHERE p.id = :parentId")
    List<Folder> findFoldersByParentId(Integer parentId);

    @Query("SELECT f FROM Folder f WHERE f.name = :name")
    Folder findByName(String name);

    @Query("SELECT f FROM Folder f WHERE f.name = 'root'")
    Folder findRoot();

    @Modifying
    @Query("UPDATE Folder f SET f.name = :name, f.parentFolder = :parentFolder WHERE f.id = :id")
    void updateFolder(Folder parentFolder, String name, Integer id);
}
