package de.sabio.mediapool.adapter;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.*;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.*;

@Service
public class S3Adapter {

    @Autowired
    AmazonS3 s3Client;

    @Value("${minio.bucket.name}")
    String defaultBucketName;

    @Value("${minio.default.folder}")
    String defaultFolder;


    /**
     * Uploads a given file to the configured Storage Instance
     * @param id the file's ID
     * @param content the file's content
     */
    public void uploadFile(String id, byte[] content){
        File file = new File(id);
        file.canWrite();
        file.canRead();
        try {
            FileOutputStream iofs = new FileOutputStream(file);
            iofs.write(content);
            PutObjectRequest request = new PutObjectRequest(defaultBucketName,id,file);
            s3Client.putObject(request);
            //close the OutputStream and delete the temporary file in working directory
            iofs.close();
            file.delete();
        } catch (Exception e){
            throw new RuntimeException(e.getMessage());
        }
    }


    /**
     * Downloads a file identified by its ID from the configured storage Instace
     * @param id the file's id
     * @return a byte array containing the file's contents
     */
    public byte[] downloadFile(String id){
        try {
            S3Object s3Object = s3Client.getObject(defaultBucketName,id);
            S3ObjectInputStream stream = s3Object.getObjectContent();
            byte[] arr = IOUtils.toByteArray(stream);
            s3Object.close();
            return arr;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Startup method, creates a new default bucket in the configured
     * storage instance if none is present
     */
    @EventListener(ApplicationReadyEvent.class)
    private void createBucketIfNotPresent(){
        if(!s3Client.doesBucketExistV2(defaultBucketName)){
            s3Client.createBucket(defaultBucketName);
        }
    }

    @PostConstruct
    public void init(){
    }
}
