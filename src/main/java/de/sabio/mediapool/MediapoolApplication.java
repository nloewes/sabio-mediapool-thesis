package de.sabio.mediapool;

import de.sabio.mediapool.service.FolderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Tag;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

@EnableSwagger2
@SpringBootApplication
public class MediapoolApplication {

	public static final String TAG_DOCUMENT = "/document - Document Metadata";
	public static final String TAG_FOLDER = "/folder - Folder Hierarchy";
	public static final String TAG_FILES = "/file - Files";

	private static final String DESC_DOCUMENT = "Operations to add, update and retrieve Document Metadata";
	private static final String DESC_FOLDER = "Operations to manage the folder structure";
	private static final String DESC_FILES = "Operations to upload and download files";


	public static void main(String[] args) {
		SpringApplication.run(MediapoolApplication.class, args);
	}

	@Bean
	public Docket swaggerConfiguration(){
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.apis(RequestHandlerSelectors.basePackage("de.sabio.mediapool.controller"))
				.build()
				.apiInfo(apiDetails())
				.tags(new Tag(TAG_DOCUMENT, DESC_DOCUMENT))
				.tags(new Tag(TAG_FOLDER, DESC_FOLDER))
				.tags(new Tag(TAG_FILES, DESC_FILES));
	}

	private ApiInfo apiDetails(){
		return new ApiInfo(
				"SABIO Mediapool API",
				"API to manage and retrieve Media Files and Documents for SABIO",
				"0.0.1",
				"-",
				new springfox.documentation.service.Contact("Nicolas Löwes","www.serviceware.de","nicolas.loewes@serviceware.de"),
				"Unlicensed Proof of Concept",
				"www.serviceware.de",
				Collections.emptyList());

	}

}
