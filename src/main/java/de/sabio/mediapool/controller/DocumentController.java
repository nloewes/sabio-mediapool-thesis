package de.sabio.mediapool.controller;

import de.sabio.mediapool.MediapoolApplication;
import de.sabio.mediapool.model.DocumentEntity;
import de.sabio.mediapool.model.DocumentResource;
import de.sabio.mediapool.service.DocumentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;

/**
 * REST Controller that provides endpoints for accessing DocumentEntities from DB.
 * Provides provides methods for creation, retrieval, search and alteration of documents
 *
 * @author nloewes
 */
@CrossOrigin(origins = "*", methods = {RequestMethod.POST,RequestMethod.GET,RequestMethod.PUT} ,exposedHeaders = {HttpHeaders.ACCESS_CONTROL_ALLOW_ORIGIN})
@Controller
@RequestMapping(path="/document")
@Api(tags = {MediapoolApplication.TAG_DOCUMENT})
public class DocumentController{

    @Autowired
    private DocumentService documentService;

    /**
     * Endpoint to create a Document entity in the database.
     * If the parentFolderId provided in the request can not be converted to Integer, the document will be created
     * at root level
     *
     * @param dto a DocumentResource containing the desired document metadata
     *
     * @return the newly created DocumentEntity
     */
    @ApiOperation(value = "Create a new Document representation",
            notes = "Create a new Document based on the provided DocumentResource. If the provided folder id can not be " +
                    "parsed to integer, the document will be created at the root level of the folder structure")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public @ResponseBody DocumentResource createDocument(@RequestBody DocumentResource dto){
        return new DocumentResource(documentService.convertAndCreateDocument(dto));
    }

    @ApiOperation(value = "Retrieve a Document by its ID")
    @GetMapping("/{id}")
    public @ResponseBody DocumentResource getDocument(@PathVariable String id){
        return new DocumentResource(documentService.getDocument(id));
    }

    /**
     * Endpoint to search for any saved document by it's title.
     *
     * Uses an SQL 'LIKE' select with pre- and suffixed wildcard
     * @param title the title to search for
     *
     * @return A list of all similar documents currently saved in the database.
     *
     */
    @ApiOperation(value = "Search all existing Documents by title",
            notes = "Provide a string to search all existing documents for matching titles")
    @GetMapping(path="/search")
    public @ResponseBody ArrayList<DocumentResource> searchByTitle(@RequestParam String title){
        ArrayList<DocumentResource> ret = new ArrayList<>();
        for(DocumentEntity doc : documentService.searchByTitle(title)){
            ret.add(new DocumentResource(doc));
        }
        return ret;
    }

    /**
     * Endpoint to update an already existing DocumentEntity according to values of a given DocumentResource
     *
     * @param dto the given DocumentResource
     *
     * @return the updated DocumentEntity converted to a DocumentResource
     */
    @ApiOperation(value = "Update an existing Document",
            notes = "Provide a DocumentResource to update an existing document with the same ID to" +
                    "the data provided in your request")
    @PutMapping
    public @ResponseBody DocumentResource updateDocument(@RequestBody DocumentResource dto){
        return new DocumentResource(documentService.updateDocument(dto));
    }

}
