package de.sabio.mediapool.controller;

import de.sabio.mediapool.MediapoolApplication;
import de.sabio.mediapool.model.DocumentEntity;
import de.sabio.mediapool.model.DocumentResource;
import de.sabio.mediapool.model.Folder;
import de.sabio.mediapool.model.FolderResource;
import de.sabio.mediapool.service.FolderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;

/**
 * REST controller that provides endpoints to access and alter the folder structure.
 * Provides methods for creating and moving folders as well as endpoints to retrieve the complete folder structure
 *
 * @author nloewes
 */
@CrossOrigin(origins = "*", methods = {RequestMethod.POST,RequestMethod.GET,RequestMethod.PUT} ,exposedHeaders = {HttpHeaders.ACCESS_CONTROL_ALLOW_ORIGIN})
@RestController
@RequestMapping(value="/folder")
@Api(tags = {MediapoolApplication.TAG_FOLDER})
public class FolderController {

    @Autowired
    private FolderService folderService;

    /**
     * Adds a new folder with a given name underneath a given a given parent folder, identified by it's ID
     * if the parameter for the parent ID is empty, the folder is created at root level
     *
     * @return An Array containing the newly created folder. The response is wrapped inside an Array to allow
     * Proxy-Components of Frontend-Frameworks like ext.js to properly
     */
    @ApiOperation(value = "Create a new Folder",
            notes = "Provide a FolderResource to create a new Folder in the existing Structure. If the ID provided in the" +
                    "FolderResource can not be parsed to int, the Folder is created at root level")
    @PostMapping("/")
    public @ResponseBody ArrayList<FolderResource> createFolder(@RequestBody FolderResource folder){
        ArrayList<FolderResource> ret = new ArrayList<>();
        ret.add(folderService.createFolder(folder));
        return ret;
    }

    /**
     * Returns a JSON Tree-Like representation of all child-folders of the folder identified by a given ID.
     *
     * @return all children of the given folder id. If no id is passed, all children of the root node are returned
     */
    @ApiOperation(value = "Retrieve the complete folder structure",
            notes = "Retrieve a tree representation of the folder structure persisted in the mediapool, starting with the " +
                    "children of the root level")
    @GetMapping(path = {"/"})
    public ResponseEntity<Object> getTree(){
        return folderService.getTree();
    }


    /**
     * Endpoint to update an existing Folder Entity based on values of a given FolderResource
     * @param folder the given FolderResource
     * @return the updated Folder Entity converted to a FolderResource
     */
    @ApiOperation(value = "Update an existing Folder",
            notes = "Provide a FolderResource to update an existing folder with the same ID to" +
                    "the data provided in your request")
    @PutMapping("/")
    public @ResponseBody FolderResource updateFolder(@RequestBody FolderResource folder){
        return folderService.updateFolder(folder);
    }

    /**
     * Endpoint to retrieve all DocumentEntities assigned to a Folder Entity, identified by its ID
     * @param folderId the ID of the folder whose children are to be retrieved

     * @return a List of all DocumentEntities assigned to the folder, converted to DocumentResources
     */
    @ApiOperation(value = "Retrieve a List of all Documents that are assigned to a folder",
            notes = "Provide a valid ID to retrieve a List containing all documents that are assigned to that folder")
    @GetMapping("/content/{folderId}")
    public @ResponseBody
    ArrayList<DocumentResource> getDocumentsByFolder(@PathVariable String folderId){
        ArrayList<DocumentResource> ret = new ArrayList<>();
        for(DocumentEntity doc : folderService.getDocumentsByFolder(folderId)){
            ret.add(new DocumentResource(doc));
        }
        return ret;
    }
}
