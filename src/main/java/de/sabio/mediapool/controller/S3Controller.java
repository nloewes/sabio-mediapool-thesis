package de.sabio.mediapool.controller;

import de.sabio.mediapool.MediapoolApplication;
import de.sabio.mediapool.adapter.S3Adapter;
import de.sabio.mediapool.model.DocumentEntity;
import de.sabio.mediapool.repository.DocumentRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@CrossOrigin(origins = "*", methods = {RequestMethod.POST,RequestMethod.GET,RequestMethod.PUT} ,exposedHeaders = {HttpHeaders.ACCESS_CONTROL_ALLOW_ORIGIN})
@RestController
@RequestMapping(value = "/file")
@Api(tags = {MediapoolApplication.TAG_FILES})
public class S3Controller {

    @Autowired
    private DocumentRepository documentRepository;

    @Autowired
    private S3Adapter s3Adapter;

    /**
     * Endpoint to upload given multipart files to the configured storage instance
     * @param files the given multipart file
     * @param fileName the desired filename
     *
     * @return a Map of <String,String> containing the key "data" and the file's original name. This return value was
     * specifically implemented for communication with the SABIO Frontend Client
     * @throws IOException if the storage instance is unavailable or cannot handle the request
     */
    @ApiOperation(value = "Upload a file",
            notes = "Provide a Multipart File and a filename to upload the file to the mediapool's storage using " +
                    "your filename as the Key. To be used after creating a corresponding document via [POST]/document")
    @PostMapping(path = "/upload", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    public @ResponseBody  Map<String, String> uploadFile(
            @RequestPart(value = "file", required = false) MultipartFile files,
            @RequestParam(value = "fileName", required = false, defaultValue = "") String fileName) throws IOException {
        s3Adapter.uploadFile(fileName, files.getBytes());
        Map<String, String> result = new HashMap<>();
        result.put("data", files.getOriginalFilename());
        return result;
    }

    /**
     * Endpoint to download a file identified by its ID from the configured storage instance
     * @param id the desired file's id
     * @return a ResponseEntity consisting of a ByteArrayResource holding the file's data
     */
    @ApiOperation(value = "Download a file",
            notes = "Provide a valid ID to download the file associated with it")
    @GetMapping("/{id}")
    public ResponseEntity<ByteArrayResource> download(@PathVariable String id){
        byte[] data = s3Adapter.downloadFile(id);
        ByteArrayResource resource = new ByteArrayResource(data);
        DocumentEntity doc = documentRepository.findById(Integer.valueOf(id)).get();

        return ResponseEntity
                .ok()
                .contentLength(data.length)
                .header("Content-type", "application/octet-stream")
                .header("Content-disposition", "attachment; filename=\"" + doc.getFilename() + "\"")
                .body(resource);
    }

}
