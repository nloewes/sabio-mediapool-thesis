package de.sabio.mediapool.service;

import de.sabio.mediapool.model.DocumentEntity;
import de.sabio.mediapool.model.Folder;
import de.sabio.mediapool.model.FolderResource;
import de.sabio.mediapool.repository.DocumentRepository;
import de.sabio.mediapool.repository.FolderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Implementation of the proxy service to prevent directly exposing the Folder entity to the consumer.
 * Provides methods to access the folder hierarchy
 *
 * @author nloewes
 *
 */
@Service
public class FolderServiceImpl implements FolderService{

    @Autowired
    FolderRepository folderRepository;

    @Autowired
    DocumentRepository documentRepository;

    /**
     * Retrieves the folder hierarchy starting from a given folder, identified by it's ID.
     *
     * @param id the folder's ID
     * @return a List of all child folders of the given folder
     */
    @Override
    @Transactional
    public List<Folder> getFolderHierarchy(Integer id){
        Folder folder = folderRepository.findById(id).get();

        traverseAndFetchChildren(folder);

        return folder.getChildren();
    }

    /**
     * Recursively traverses the children of a given folder and fetches all children of children and so forth.
     *
     * @param folder the folder whose children are to be fetched
     */
    private void traverseAndFetchChildren(Folder folder){
        //make the persistence context lazily load the @OneToMany dependencies
        int size = folder.getChildren().size();
        if(size > 0){
            for (Folder childFolder : folder.getChildren()){
                traverseAndFetchChildren(childFolder);
            }
        }
    }

    /**
     * Startup method, checks whether there is already a root node for the folder structure in the database.
     * If it exists, no action is taken.
     * If it does not exist a new node called "root" is created.
     */
    @EventListener(ApplicationReadyEvent.class)
    protected void createRootIfNotPresent(){
        if(folderRepository.findByName("root") == null){
            Folder root = new Folder();
            root.setParentFolder(null);
            root.setName("root");
            folderRepository.save(root);
        }
    }

    /**
     * Generates a Tree representation of all Children of the root folder in the folderRepository.
     * Children are sorted by their date of creation to ensure user-friendly representation
     *
     * @return a ResponseEntity consisting of the generated Tree structure
     */
    @Override
    public ResponseEntity<Object> getTree(){
        Folder folder;
        folder = folderRepository.findRoot();

        if(folder == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        List<Folder> folders = getFolderHierarchy(folder.getId());
        if(folders == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            folders.sort(Comparator.comparing(Folder::getCreated));
        }

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        return new ResponseEntity<>(folders, headers, HttpStatus.OK);
    }

    /**
     * Updates an existing Folder Entity based on the data of a given FolderResource
     * @param folder the given FolderResource
     * @return the updated Folder entity
     */
    @Override
    @Transactional
    public FolderResource updateFolder(FolderResource folder){
        Folder parent;
        if(folder.getParentId().equals("root")){
            parent = folderRepository.findRoot();
        } else {
            parent = folderRepository.findById(Integer.valueOf(folder.getParentId())).get();
        }
        folderRepository.updateFolder(parent, folder.getName(), Integer.valueOf(folder.getId()));
        return folder;
    }

    /**
     * Creates a new Folder Entity in the Folder Repository based the data of a given FolderResource
     * If the FolderResource contained an unusable ID value, the folder is created as a child of the root node
     * @param folder the given FolderResource
     * @return the given FolderResource
     */
    @Override
    public FolderResource createFolder(FolderResource folder){
        Folder add = new Folder();
        add.setName(folder.getName());
        if(folder.getParentId().equals("root")){
            add.setParentFolder(folderRepository.findRoot());
        } else {
            add.setParentFolder(folderRepository.findById(Integer.valueOf(folder.getParentId())).get());
        }
        add.setCreated(LocalDateTime.now());
        folderRepository.save(add);
        folder.setId(add.getId().toString());
        return folder;
    }

    /**
     * Generates a list of all DocumentEntities that are assigned to a Folder Entity identified by it's ID
     * @param folderId the ID of the folder whose children are to be retrieved
     * @return a List of DocumentEntities that are assigned to the Folder Entity, empty list if there are no children
     */
    @Override
    public ArrayList<DocumentEntity> getDocumentsByFolder(String folderId){
        if(folderId.equals("root")){
            return documentRepository.findByParent(folderRepository.findRoot());
        } else {
            return documentRepository.findByParent(folderRepository.findById(Integer.valueOf(folderId)).get());
        }
    }
}
