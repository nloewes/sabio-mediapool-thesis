package de.sabio.mediapool.service;

import de.sabio.mediapool.model.DocumentEntity;
import de.sabio.mediapool.model.DocumentResource;
import de.sabio.mediapool.repository.DocumentRepository;
import de.sabio.mediapool.repository.FolderRepository;
import de.sabio.mediapool.utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;

/**
 * Proxy service to prevent directly exposing the DocumentEntity to the consumer.
 * Provides methods to add, update and retrieve DocumentEntities from the DocumentRepository
 *
 * @author nloewes
 */
@Service
public class DocumentServiceImpl implements DocumentService{

    @Autowired
    DocumentRepository documentRepository;

    @Autowired
    FolderRepository folderRepository;

    /**
     * Converts a given DocumentResource to a DocumentEntity and adds it to the DocumentRepository
     * @param dto the given DocumentResource
     * @return the newly created DocumentEntity
     */
    @Override
    public DocumentEntity convertAndCreateDocument(DocumentResource dto){
        DocumentEntity doc = new DocumentEntity();
        //if the DocumentEntity provided in the request did not contain a parentID, set it to the current root.
        if (dto.getParentFolderId() == null || !utils.isNumeric(dto.getParentFolderId())){
            doc.setParent(folderRepository.findRoot());
        } else {
            doc.setParent(folderRepository.findById(Integer.valueOf(dto.getParentFolderId())).get());
        }
        doc.setFilename(dto.getFilename());
        doc.setTitle(dto.getTitle());
        doc.setCreated(LocalDateTime.now());
        doc.setLastModified(LocalDateTime.now());
        //save document entity via repository to create ID.
        documentRepository.save(doc);
        return doc;
    }

    /**
     * Retrieves a DocumentEntity identified by a given ID
     * @param id the given ID
     * @return the desired DocumentEntity or null of it does not exist
     */
    @Override
    public DocumentEntity getDocument(String id){
        DocumentEntity doc = documentRepository.findById(Integer.valueOf(id)).get();
        return doc;
    }

    /**
     * Searches the DocumentRepository for all Titles containing a likeness of a given string
     * @param title the given string
     * @return a List of all matching DocumentEntities
     */
    @Override
    public ArrayList<DocumentEntity> searchByTitle(String title){
        return documentRepository.findByTitleLike("%" + title + "%");
    }

    /**
     * Updates an existing DocumentEntity based on the data of a given DocumentResource
     * @param dto the given DocumentResource
     * @return the updated DocumentEntity
     */
    @Override
    public DocumentEntity updateDocument(DocumentResource dto){
        DocumentEntity toUpdate = documentRepository.findById(dto.getId()).get();
        toUpdate.setParent(folderRepository.findById(Integer.valueOf(dto.getParentFolderId())).get());
        toUpdate.setTitle(dto.getTitle());
        toUpdate.setFilename(dto.getFilename());
        documentRepository.save(toUpdate);
        return toUpdate;
    }
}
