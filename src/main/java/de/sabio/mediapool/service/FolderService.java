package de.sabio.mediapool.service;

import de.sabio.mediapool.model.DocumentEntity;
import de.sabio.mediapool.model.Folder;
import de.sabio.mediapool.model.FolderResource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.ArrayList;
import java.util.List;

/**
 * Proxy service to prevent directly exposing the Folder entity to the consumer.
 * Provides methods to access the folder hierarchy
 *
 * @author nloewes
 */
public interface FolderService {

    List<Folder> getFolderHierarchy(Integer id);

    ResponseEntity<Object> getTree();

    FolderResource createFolder(FolderResource folder);

    ArrayList<DocumentEntity> getDocumentsByFolder(@PathVariable String folderId);

    FolderResource updateFolder(FolderResource folder);

}
