package de.sabio.mediapool.service;

import de.sabio.mediapool.model.DocumentEntity;
import de.sabio.mediapool.model.DocumentResource;

import java.util.ArrayList;

/**
 * Implemented by DocumentServiceImpl
 *
 * @author nloewes
 */
public interface DocumentService {

    DocumentEntity getDocument(String id);

    DocumentEntity convertAndCreateDocument(DocumentResource dto);

    ArrayList<DocumentEntity> searchByTitle(String title);

    DocumentEntity updateDocument(DocumentResource document);
}
