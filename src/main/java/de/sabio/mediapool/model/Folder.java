package de.sabio.mediapool.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.sun.istack.NotNull;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Base entity for representing a folder structure.
 * Each Folder has a name and a parent as well as a list of children.
 * Children may be null, while only one folder may have null as a parent.
 * This particular folder represents the 'root' folder for each folder structure.
 *
 * @author nloewes
 *
 */
@Entity
public class Folder {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer id;

    @NotNull
    private String name;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name="parent_id")
    private Folder parentFolder;

    @OneToMany(mappedBy = "parentFolder", cascade = {CascadeType.REMOVE, CascadeType.PERSIST})
    private List<Folder> children;

    public LocalDateTime created;

    public LocalDateTime lastModified;

    public LocalDateTime getLastModified() {
        return lastModified;
    }

    public void setLastModified(LocalDateTime lastModified) {
        this.lastModified = lastModified;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }


    public List<Folder> getChildren() {
        return children;
    }

    public void setChildren(List<Folder> children) {
        this.children = children;
    }

    public Folder getParentFolder() {
        return parentFolder;
    }

    public void setParentFolder(Folder parentFolder) {
        this.parentFolder = parentFolder;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
