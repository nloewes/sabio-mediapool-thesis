package de.sabio.mediapool.model;

import io.swagger.annotations.ApiModelProperty;

/**
 * Lightweight resource for reception and propagation of folders via API
 *
 * @author nloewes
 */
public class FolderResource {

    @ApiModelProperty(notes = "The database generated ID of the folder")
    private String id;

    @ApiModelProperty(notes = "The name of the folder")
    private String name;

    @ApiModelProperty(notes = "The database generated ID of the parent folder of the folder")
    private String parentId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

}
