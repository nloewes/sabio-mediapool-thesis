package de.sabio.mediapool.model;

import io.swagger.annotations.ApiModelProperty;

import java.time.LocalDateTime;

/**
 * Data Transfer Object (DTO) for the Document Entity
 *
 * @author nloewes
 */


public class DocumentResource {

    @ApiModelProperty(notes = "The database generated document ID")
    private int id;

    @ApiModelProperty(notes = "The title of the document")
    private String title;

    @ApiModelProperty(notes = "The name of the file associated with this document")
    private String filename;

    @ApiModelProperty(notes = "The database generated ID of the folder the document is a child of")
    private String parentFolderId;

    @ApiModelProperty(notes = "The database generated date the document was created")
    private LocalDateTime created;

    @ApiModelProperty(notes = "The database generated date the document was last modified")
    private LocalDateTime lastModified;

    public DocumentResource(){
    }

    public DocumentResource(DocumentEntity doc){
        this.filename = doc.getFilename();
        this.id = doc.getId();
        this.title = doc.getTitle();
        this.parentFolderId = doc.getParent().getId().toString();
        this.created = doc.getCreated();
        this.lastModified = doc.getLastModified();
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public LocalDateTime getLastModified() {
        return lastModified;
    }

    public void setLastModified(LocalDateTime lastModified) {
        this.lastModified = lastModified;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getParentFolderId() {
        return parentFolderId;
    }

    public void setParentFolderId(String parentFolderId) {
        this.parentFolderId = parentFolderId;
    }
}
