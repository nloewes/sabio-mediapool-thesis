# SABIO MEDIAPOOL

This file contains general instructions on how to build and run the Proof-of-Concept implementation of the SABIO Mediapool, created as part of the Bachelor's thesis of @nloewes


# Prerequisites

 - JDK 8 or higher
 - Docker Engine installed
 - Docker-Compose installed 


# Building the application

Windows:

    mvnw verify -DskipTests

UNIX:

    ./mvn verify -DskipTests

# Running the Application as a Docker Container

    docker-compose up --build


