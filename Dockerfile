FROM openjdk:8-jdk-alpine
MAINTAINER nloewes
COPY ./target/mediapool-0.0.1-SNAPSHOT.jar /usr/src/mediapool/
WORKDIR /usr/src/mediapool
EXPOSE 8050
CMD ["java", "-jar", "mediapool-0.0.1-SNAPSHOT.jar"]